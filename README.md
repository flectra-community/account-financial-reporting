# Flectra Community / account-financial-reporting

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[account_financial_report](account_financial_report/) | 2.0.2.4.0| OCA Financial Reports
[account_move_line_report_xls](account_move_line_report_xls/) | 2.0.1.0.1| Journal Items Excel export
[account_tax_balance](account_tax_balance/) | 2.0.1.2.5| Compute tax balances based on date range
[mis_template_financial_report](mis_template_financial_report/) | 2.0.1.0.0| Profit & Loss / Balance sheet MIS templates
[mis_builder_cash_flow](mis_builder_cash_flow/) | 2.0.1.0.0| MIS Builder Cash Flow
[partner_statement](partner_statement/) | 2.0.1.2.0| OCA Financial Reports


