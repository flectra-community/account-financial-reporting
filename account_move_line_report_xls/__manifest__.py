# Copyright 2009-2020 Noviat.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Account Move Line XLSX export",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "author": "Noviat, Odoo Community Association (OCA)",
    "category": "Accounting & Finance",
    "website": "https://gitlab.com/flectra-community/account-financial-reporting",
    "summary": "Journal Items Excel export",
    "depends": ["account", "report_xlsx_helper"],
    "data": ["report/account_move_line_xlsx.xml"],
    "installable": True,
}
